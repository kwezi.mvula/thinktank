<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class, 'index'])->name('home');
Route::get('/add/user', [UserController::class, 'create']);
Route::post('/save_user', [UserController::class, 'store'])->name('save_user');
Route::get('/edit/user/{id}',[UserController::class, 'edit']);
Route::post('update_user/{id}',[UserController::class,'update'])->name('update_user');
Route::post('delete_user/{id}',[UserController::class,'destroy'])->name('delete_user');

