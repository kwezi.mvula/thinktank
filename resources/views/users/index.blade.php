@extends('layouts.app')
@section('content')
<!--app-content open-->
<div class="main-content app-content mt-0">
    <div class="side-app">

        <!-- container -->
        <div class="main-container container-fluid">


            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Users</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Users</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- ROW-4 -->
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title mb-0">Users</h3>
                        </div>
                        <div class="card-body pt-4">
                            <div class="grid-margin">
                                <div class="">
                                    <div class="panel panel-primary">
                                        <div class="tab-menu-heading border-0 p-0">

                                            <div class="tabs-menu1">
                                                <!-- Tabs -->
                                                <ul class="nav panel-tabs product-sale">
                                                    <a class="btn text-secondary btn-md" href="/add/user" data-bs-toggle="tooltip" data-bs-original-title="Add"><span class="fe fe-plus-square fs-25"></span></a>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="panel-body tabs-menu-body border-0 pt-0">
                                            @if ($message = Session::get('success'))
                                            <div class="alert alert-success">
                                                <p>{{ $message }}</p>
                                            </div>
                                            @endif
                                            @if ($message = Session::get('delete'))
                                            <div class="alert alert-danger">
                                                <p>{{ $message }}</p>
                                            </div>
                                            @endif

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab5">
                                                    <div class="table-responsive">
                                                        <table id="data-table" class="table table-bordered text-nowrap mb-0">
                                                            <thead class="border-top">
                                                                <tr>
                                                                    <th class="bg-transparent border-bottom-0">
                                                                        Name</th>
                                                                    <th class="bg-transparent border-bottom-0">
                                                                        Surname</th>
                                                                    <th class="bg-transparent border-bottom-0">
                                                                        Email</th>
                                                                    <th class="bg-transparent border-bottom-0">
                                                                        Phone Number</th>
                                                                    <th class="bg-transparent border-bottom-0">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($users as $user)
                                                                <tr class="border-bottom">
                                                                    <td><span class="fw-semibold mt-sm-2 d-block">{{ $user->name }}</span>
                                                                    </td>
                                                                    <td><span class="fw-semibold mt-sm-2 d-block">{{ $user->surname }}</span>
                                                                    </td>
                                                                    <td><span class="fw-semibold mt-sm-2 d-block">{{ $user->email }}</span>
                                                                    </td>
                                                                    <td><span class="fw-semibold mt-sm-2 d-block">{{ $user->contact_number }}</span>
                                                                    </td>
                                                                    <td>
                                                                        <div class="g-2">
                                                                            <a class="modal-effect btn text-secondary btn-lg" data-bs-effect="effect-newspaper" data-bs-toggle="modal" data-bs-original-title="View" href="#user_details{{$user->id}}"><span class="fe fe-eye fs-30"></span></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-6 col-sm-6">
                                            {{ $users->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ROW-4 END -->


        </div>
        <!-- container-closed -->
    </div>
</div>
<!--app-content closed-->
</div>
<!-- page-main closed -->

<!--app-modals-->
@include('includes.user_details')

<!--app-modals-->

@endsection