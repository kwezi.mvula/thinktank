@extends('layouts.app')
@section('content')
<!--app-content open-->
<div class="main-content app-content mt-0">
    <div class="side-app">
        <!-- container -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Users</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit User</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- Row -->
            <div class="row ">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header border-bottom-0">
                            <div class="card-title">

                            </div>
                        </div>
                        <div class="card-body">
                            <div id="wizard1">
                                <h3>Edit User Information</h3>
                                @if ($errors->any())
                                <div class="container pt-4">
                                    @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger" role="alert">
                                        <div class="iq-alert-icon">
                                            <i class="ri-alert-line"></i>
                                        </div>
                                        <div class="iq-alert-text">
                                            <li>{{ $error }}</li>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                @endif
                                <form method="POST" action="{{ route('update_user',$user->id) }}" enctype="multipart/form-data">
                                    @csrf
                                    <section>
                                        <div class="form-group">
                                            <div class="row ">
                                                <div class="col-md-7 col-lg-6">
                                                    <input class="form-control" id="name" name="name" value="{{$user->name}}" placeholder="{{$user->name}}" type="text"  type="text">
                                                </div>
                                                <div class="col-md-7 col-lg-6 mg-t-20 mg-md-t-0">
                                                    <input class="form-control" id="surname" name="surname" value="{{$user->surname}}" placeholder=" {{$user->surname}}"  type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="form-group">
                                            <div class="row ">
                                                <div class="col-md-7 col-lg-6">
                                                <input class="form-control" id="email" name="email" value="{{ $user->email }}" placeholder="{{$user->email}}"  type="email">
                                                </div>
                                                <div class="col-md-7 col-lg-6 mg-t-20 mg-md-t-0">
                                                    <input class="form-control" id="contact_number" name="contact_number" value="{{$user->contact_number}}" type="text"placeholder="{{ $user->contact_number}}" type="number">
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="form-group">
                                            <div class="row ">
                                                <div class="col-md-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <form>
                                                                <button type="submit" class="btn btn-primary py-1 px-4 mb-1">Submit</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Row -->
        </div>
        <!-- container-closed -->
    </div>
</div>
<!--app-content closed-->
@endsection