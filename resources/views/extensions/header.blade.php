<!-- app-Header -->
<div class="app-header header sticky">
    <div class="container-fluid main-container">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="d-flex order-lg-2 ms-auto header-right-icons">
                <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-bs-toggle="sidebar" href="javascript:void(0)"></a>
                <a class="logo-horizontal " href="/">
                    <img src="{{ asset('assets/images/brand/logo.png') }}"class="header-brand-img desktop-logo" alt="logo">
                    <img src="{{ asset('assets/images/brand/logo-3.png') }}" class="header-brand-img light-logo1" alt="logo">
                </a>
                <!-- LOGO -->
                <!-- sidebar-toggle-->
                <!-- Theme-Layout -->
                <div class="dropdown d-flex">
                    <a class="nav-link icon full-screen-link nav-link-bg">
                        <i class="fe fe-minimize fullscreen-button"></i>
                    </a>
                </div>
                

             
            </div>
        </div>
    </div>
   
</div>
<!-- /app-Header -->
