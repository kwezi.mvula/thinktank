<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'ThinkTank') }}</title>
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/brand/favicon.ico') }}" />

    <!-- BOOTSTRAP CSS -->
    <link id="style" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- STYLE CSS -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/dark-style.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/transparent-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/skin-modes.css') }}" rel="stylesheet" />

    <!--- FONT-ICONS CSS -->
    <link href="{{ asset('assets/plugins/icons/icons.css') }}" rel="stylesheet" />

    <!-- COLOR SKIN CSS -->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{ asset('assets/css/color1.css') }}" />

    <!-- INTERNAL Switcher css -->
    <link href="{{ asset('assets/switcher/css/switcher.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/switcher/demo.css') }}" rel="stylesheet" />


</head>

<body class="app sidebar-mini ltr">
    <!-- Switcher -->
    @include('extensions.switcher')
    <!-- End Switcher -->
    <!-- global-loader -->
    <div id="global-loader">
        <img src="{{ asset('assets/images/loader.svg') }}" class="loader-img" alt="Loader">
    </div>
    <!-- global-loader closed -->

    <!-- page -->
    <div class="page">
        <div class="page-main">

            <!-- app-Header -->
            @include('extensions.header')
            <!-- /app-Header -->

            <!--App-Sidebar-->
            @include('extensions.sidebar')
            <!--/App-Sidebar-->

            <!--app-content open-->
            @yield('content')
            <!--app-content open-->       

            <!-- FOOTER -->
            @include('extensions.footer')
            <!-- FOOTER CLOSED -->

        </div>
        <!-- page -->

        <!-- BACK-TO-TOP -->
        <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

        <!-- JQUERY JS -->
        <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>

        <!-- BOOTSTRAP JS -->
        <script src="{{ asset('assets/plugins/bootstrap/js/popper.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>

        <!-- SHOW PASSWORD JS -->
        <script src="{{ asset('assets/plugins/show-password/show-password.min.js') }}"></script>

        <!-- GENERATE OTP JS -->
        <script src="{{ asset('assets/js/generate-otp.js') }}"></script>

        <!-- Perfect SCROLLBAR JS-->
        <script src="{{ asset('assets/plugins/p-scroll/perfect-scrollbar.js') }}"></script>

        <!-- INPUT MASK JS -->
        <script src="{{ asset('assets/plugins/input-mask/jquery.mask.min.js') }}"></script>

        <!-- Color Theme js -->
        <script src="{{ asset('assets/js/themeColors.js') }}"></script>

        <!-- CUSTOM JS -->
        <script src="{{ asset('assets/js/custom.js') }}"></script>

        <!-- Switcher js -->
        <script src="{{ asset('assets/switcher/js/switcher.js') }}"></script>


        <!-- SIDE-MENU JS -->
        <script src="{{ asset('assets/plugins/sidemenu/sidemenu.js') }}"></script>

        <!-- SIDEBAR JS -->
        <script src="{{ asset('assets/plugins/sidebar/sidebar.js') }}"></script>

        <!-- Perfect SCROLLBAR JS-->
        <script src="{{ asset('assets/plugins/p-scroll/pscroll.js') }}"></script>
        <script src="{{ asset('assets/plugins/p-scroll/pscroll-1.js') }}"></script>


        <!-- SPARKLINE JS-->
        <script src="{{ asset('assets/plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>

        <!-- CHART-CIRCLE JS-->
        <script src="{{ asset('assets/plugins/circle-progress/circle-progress.min.js') }}"></script>

        <!-- INTERNAL SELECT2 JS -->
        <script src="{{ asset('assets/plugins/select2/select2.full.min.js') }}"></script>
        <script src="{{ asset('assets/js/select2.js') }}"></script>

        <!-- PIETY CHART JS-->
        <script src="{{ asset('assets/plugins/peitychart/jquery.peity.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/peitychart/peitychart.init.js') }}"></script>

        <!-- INTERNAL CHARTJS CHART JS-->
        <script src="{{ asset('assets/plugins/chart/Chart.bundle.js') }}"></script>
        <script src="{{ asset('assets/plugins/chart/rounded-barchart.js') }}"></script>
        <script src="{{ asset('assets/plugins/chart/utils.js') }}"></script>

        <!-- INTERNAL SELECT2 JS -->
        <script src="{{ asset('assets/plugins/select2/select2.full.min.js') }}"></script>

        <!-- INTERNAL Data tables js-->
        <script src="{{ asset('assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatable/js/dataTables.bootstrap5.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>

        <!-- INTERNAL APEXCHART JS -->
        <script src="{{ asset('assets/js/apexcharts.js') }}"></script>
        <script src="{{ asset('assets/plugins/apexchart/irregular-data-series.js') }}"></script>

        <!-- C3 CHART JS -->
        <script src="{{ asset('assets/plugins/charts-c3/d3.v5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/charts-c3/c3-chart.js') }}"></script>

        <!-- CHART-DONUT JS -->
        <script src="{{ asset('assets/js/charts.js') }}"></script>

        <!-- INTERNAL Flot JS -->
        <script src="{{ asset('assets/plugins/flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('assets/plugins/flot/jquery.flot.fillbetween.js') }}"></script>
        <script src="{{ asset('assets/plugins/flot/chart.flot.sampledata.js') }}"></script>
        <script src="{{ asset('assets/plugins/flot/dashboard.sampledata.js') }}"></script>

        <!-- INTERNAL Vector js -->
        <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>

        <!-- INTERNAL INDEX JS -->
        <script src="{{ asset('assets/js/index.js') }}"></script>
        <script src="{{ asset('assets/js/index1.js') }}"></script>
        <script src="{{ asset('assets/js/index2.js') }}"></script>


        <!-- Sticky js -->
        <script src="{{ asset('assets/js/sticky.js') }}"></script>

        <!-- DEFAULT CALENDAR JS-->
        <script src="{{ asset('assets/plugins/calendar/underscore-min.js') }}"></script>
        <script src="{{ asset('assets/plugins/calendar/moment.js') }}"></script>
        <script src="{{ asset('assets/plugins/calendar/calendar.js') }}"></script>
        <script src="{{ asset('assets/plugins/calendar/defaultcalendar.js') }}"></script>

        <!-- INTERNAL WYSIWYG Editor JS -->
        <script src="{{ asset('assets/plugins/wysiwyag/jquery.richtext.js') }}"></script>
        <script src="{{ asset('assets/plugins/wysiwyag/wysiwyag.js') }}"></script>

        <!-- Color Theme js -->
        <script src="{{ asset('assets/js/themeColors.js') }}"></script>

        <!-- Sticky js -->
        <script src="{{ asset('assets/js/sticky.js') }}"></script>

        <!-- CUSTOM JS -->

        <!-- Switcher js -->
        <script src="{{ asset('assets/switcher/js/switcher.js') }}"></script>


</body>

</html>