@foreach($users as $user)
<div class="modal fade" id="user_details{{$user->id}}">
    <div class="modal-dialog modal-lg modal-dialog-centered text-center" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h6 class="modal-title">User Information</h6><button aria-label="Close" class="btn-close" data-bs-dismiss="modal"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="media mt-0">
                            <div class="media-user me-2">
                                <div class="">
                                    <div class="media-icon bg-success text-white mb-3 mb-sm-0 me-3 mt-1">
                                        <svg style="width:24px;height:24px;margin-top:-8px" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M20,6C20.58,6 21.05,6.2 21.42,6.59C21.8,7 22,7.45 22,8V19C22,19.55 21.8,20 21.42,20.41C21.05,20.8 20.58,21 20,21H4C3.42,21 2.95,20.8 2.58,20.41C2.2,20 2,19.55 2,19V8C2,7.45 2.2,7 2.58,6.59C2.95,6.2 3.42,6 4,6H8V4C8,3.42 8.2,2.95 8.58,2.58C8.95,2.2 9.42,2 10,2H14C14.58,2 15.05,2.2 15.42,2.58C15.8,2.95 16,3.42 16,4V6H20M4,8V19H20V8H4M14,6V4H10V6H14M12,9A2.25,2.25 0 0,1 14.25,11.25C14.25,12.5 13.24,13.5 12,13.5A2.25,2.25 0 0,1 9.75,11.25C9.75,10 10.76,9 12,9M16.5,18H7.5V16.88C7.5,15.63 9.5,14.63 12,14.63C14.5,14.63 16.5,15.63 16.5,16.88V18Z" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0 mt-1">{{$user->name}} {{$user->surname}}</h6>
                                <small class="text-muted">{{$user->created_at}}</small>
                            </div>
                        </div>
                        <div class="ms-auto">
                            <div class="dropdown show">
                                <a class="new option-dots" href="JavaScript:void(0);" data-bs-toggle="dropdown">
                                    <span class=""><i class="fe fe-more-vertical"></i></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <a class="dropdown-item" href="{{ url('/edit/user/'.$user->id) }}">Edit User</a>
                                    <form action="{{ url('delete_user',$user->id) }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <button type="submit" class="btn text-default dropdown-item" >Delete User</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4">
                        <h4 class="fw-semibold mt-3">Contact Details</h4>
                        <p class="mb-0"><span class=""><i class="fe fe-mail"></i></span> {{$user->email}}
                        </p>
                        <p class="mb-0"><span class=""><i class="fe fe-phone"></i></span> {{$user->contact_number}}
                        </p>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <form action="{{ url('delete_user',$user->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <button type="submit" class="btn btn-danger" data-bs-toggle="tooltip" data-bs-original-title="Delete"><span class="fe fe-trash-2 fs-18"></span></button>
                </form>
                <a href="{{ url('/edit/user/'.$user->id) }}" class="btn btn-warning"><span class="fe fe-edit fs-18"></span></a>
                <button class="btn btn-success" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endforeach